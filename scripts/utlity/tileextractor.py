import numpy as np
import cv2
import openslide
import time


class TileExtractor:
    DEFAULT_MIN_NON_BLANK_AMT = 0.6


    def __init__(self, slide, tile_size=1024, desired_tile_mpp=0.5040):
        '''
        Creates a tile extractor object for the given slide

        Default MPP used is for 20x magnification. Regardless of the the MPP of the slide given is, the tiles produced
        will be of the `desired_tile_mpp`

        If desired MPP and slide MPP match, then this serves as a raw tile extractor.
        If they don't match:
            if slide MPP is smaller (larger magnification), takes a larger tile size and resizes the tile down
            if slide MPP is larger (smaller magnification), takes a smaller tile size and resizes the tile up

        :param slide: slide object
        :param tile_size:
        :param desired_tile_mpp: the mpp of tiles that the tile extractor returns
        '''

        self.slide = slide
        self.original_tile_size = tile_size
        self.desired_tile_mpp = desired_tile_mpp

        # resize tile size if required
        factor = desired_tile_mpp / slide.mpp if slide.mpp else 1
        modified_tile_size = int(tile_size * factor)
        self.tile_size_resize_factor = factor
        self.modified_tile_size = modified_tile_size

        # 'Crop' leftover from right and bottom
        self.trimmed_width = slide.width - (slide.width % modified_tile_size)
        self.trimmed_height = slide.height - (slide.height % modified_tile_size)
        self.chn = 3


    def iterate_tiles(self, print_time=True):
        '''
        A generator that iterates over all the tiles within the supplied slide

        :param print_time: for printing out how many tiles/how many to go
        :return: dict containing tile and coordinates
        '''

        # initialization
        x = y = 0
        tile_size = self.modified_tile_size

        # For timing and count tiles
        cols = self.trimmed_width / tile_size
        rows = self.trimmed_height / tile_size
        tot_tiles = cols * rows
        start_time = time.clock()

        # break out when top left coordinate of next tile is the bottom of image
        while y != self.trimmed_height:

            # Get current sub-image
            if isinstance(self.slide.image, openslide.OpenSlide):
                tile = np.array(self.slide.image.read_region((x, y), 0, (tile_size, tile_size)))[:, :, 2::-1]
            else:
                tile = np.array(self.slide.image.crop((x, y, x + tile_size, y + tile_size)))[:, :, 2::-1]

            r = 1 / self.tile_size_resize_factor
            if r != 1:
                tile = cv2.resize(tile, (0, 0), fx=r, fy=r)

            top_left_x, top_left_y = int(x * r), int(y * r)
            bot_right_x, bot_right_y = int((x + tile_size) * r), int((y + tile_size) * r)

            yield {
                'tile': tile,
                'coordinate': (top_left_x, top_left_y, bot_right_x, bot_right_y)
            }

            # move onto next spot
            x += tile_size
            if x == self.trimmed_width:
                x = 0
                y += tile_size

                if print_time:
                    print("{:0.2f}% ({}/{} tiles) in {:0.2f}s".format(
                        (y / tile_size) / rows * 100,  # percent of rows complete
                        (y / tile_size) * cols,  # number of rows complete * tiles per row
                        tot_tiles,
                        time.clock() - start_time))
