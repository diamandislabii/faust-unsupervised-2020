import numpy as np
import cv2


class ImageCreator:


    def __init__(self, height, width, scale_factor=1, channels=3):
        '''

        Creates an image of the specified size except scaled down by an optional factor amount

        :param height:
        :param width:
        :param scale_factor: scale the created image down by a factor of this amount
        :param channels:
        '''

        # this matrix will always have the specified dtype
        self.image = np.ones((int(height / scale_factor), int(width / scale_factor), channels), dtype=np.uint8) * 255
        self.scale_factor = scale_factor


    def _get_scaled_coordinate(self, coordinate):
        return tuple(int(c / self.scale_factor) for c in coordinate)


    def add_tile(self, tile, coordinate):
        '''

        :param tile: a 0-255 valued matrix
        :param coordinate: tuple containing top left and bottom right coordinate of image
        :return:
        '''

        # adjust coordinates depending on if we want to scale our image
        x1_adj, y1_adj, x2_adj, y2_adj = self._get_scaled_coordinate(coordinate)

        # Put sub-image into correct spot of matrix (recreating image) by resizing tile if needed to fit within the spot
        self.image[y1_adj:y2_adj, x1_adj:x2_adj, :] = cv2.resize(tile, (x2_adj - x1_adj, y2_adj - y1_adj))


    @staticmethod
    def add_border(a, thickness=0.05, color=(0, 0, 0)):
        '''

        :param a: the matrix image
        :param thickness: border thickness
        :return:
        '''

        h, w, c = a.shape

        # some coordinates may be part of cropped part of heatmap (recall we do tile size * divide fac). ignore those ones
        if h == 0 or c == 0:
            return

        if c != 3:
            raise Exception('Only RGB images supported')

        pixel_len = min(int(w * thickness), int(h * thickness))

        # for each row in the image
        for j in range(h):
            # if we are in first 5% or last% of rows, we color the whole row
            if j <= pixel_len or j >= w - pixel_len:
                # color entire row
                for i in range(3):
                    a[j, :, i] = color[i]
            else:

                # color the leftmost and rightmost 5% of the row
                for i in range(3):
                    a[j, :pixel_len, i] = color[i]
                    a[j, (w - pixel_len):, i] = color[i]


    def add_borders(self, coordinates, color=(0, 255, 0)):
        '''
        Adds colored borders onto the image at the coordinates. Default is bright green

        :param coordinates: tuple containing top left and bottom right coordinate of image
        :param color: BGR tuple
        :return:
        '''

        for idx, coordinate in enumerate(coordinates):
            # adjust coordinates depending on if we want to scale our image
            x1_adj, y1_adj, x2_adj, y2_adj = self._get_scaled_coordinate(coordinate)

            curr_slice = self.image[y1_adj:y2_adj, x1_adj:x2_adj, :]

            ImageCreator.add_border(curr_slice, thickness=0.1, color=color)
