import numpy as np
import keras.backend as K


class ModelUtils:


    @staticmethod
    def get_conf_score(model, img):
        '''
        Returns confidence score prediction of the image

        :param model: keras model
        :param img: a numpy image matrix
        :return: 1d numpy array
        '''

        img = ModelUtils._prepare_image(img)
        return model.predict(img)[0].flatten()


    @staticmethod
    def get_layer_data(model, img, layer_name=None, layer=None):
        '''
        Returns image data after the specified layer

        :param model: keras model
        :param img: a numpy image matrix
        :param layer_name: string layer name within the model
        :param layer: keras layer within the model
        :return: numpy array
        '''

        if layer_name is None and layer is None:
            raise Exception('No layer name nor layer supplied')
        elif layer_name is not None and layer is not None:
            raise Exception('Vague. Please supply either a layer name or a layer')
        elif layer is None:
            # we are given a layer name
            layer = ModelUtils._get_layer(model, layer_name)
        else:
            # we are given a layer. no need to do anything
            pass

        img = ModelUtils._prepare_image(img)

        # Variables for easy access to needed layers
        input = model.layers[0].input
        output = layer.output

        # A function that takes in the input layer and outputs data after the given layer
        get_output = K.function([input], [output])

        # run function
        return get_output([img])[0].squeeze()


    @staticmethod
    def _prepare_image(img):
        '''
        Returns a prepared image for model use

        :param img: a numpy image matrix
        :return: 4d numpy image matrix
        '''
        return np.expand_dims(img, axis=0) / 255


    @staticmethod
    def _get_layer(model, layer_name):
        '''
        Returns layer within model which has the specified layer name

        :param model: keras model
        :param layer_name: string layer name within the model
        :return:
        '''

        return model.get_layer(name=layer_name)
