# Slide Colortiling


Clusters the tiles of a slide together into k groups and draws colored boxes around each tile according to the cluster they were grouped in

Specify at the bottom of tile_clustering_in_slide.py the following key parameters:

- The k-values specify a list of how many clusters should be used (heavy lifting will be done prior to this step)

- The min_non_blank_amount and blank_index fields act as a tile filter so that blank tiles are ignored. blank_index is the index of the model's blank class prediction output.

Output is a colorcoded copy of the slide, one per k value