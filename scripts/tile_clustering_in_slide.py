import os
import cv2
import re
import pathlib
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
from keras.models import load_model

from .utlity import unique_colors
from .utlity.image_creator import ImageCreator
from .utlity.model_utils import ModelUtils
from .utlity.slide import Slide
from .utlity.tileextractor import TileExtractor


class SlideColortiler:


    def __init__(self, slide, out_dir, tile_size=1024):

        self.slide = slide
        self.out_dir = out_dir
        self.tile_size = tile_size

        # scale trimmed dimensions according to how we scaled our tile size
        te = TileExtractor(slide, tile_size)
        r = te.tile_size_resize_factor
        image_creator = ImageCreator(
            height=te.trimmed_height / r,
            width=te.trimmed_width / r,
            scale_factor=16,  # make resulting image size smaller
            channels=te.chn
        )
        self.image_creator = image_creator

        self._initialize()


    def _initialize(self):
        '''
        This is the time consuming step. Adding the actual colors is fast

        :return:
        '''

        print('Initializing image creator...please be patient')

        te = TileExtractor(self.slide, self.tile_size)

        # first add the tiles to the blank image
        for res in te.iterate_tiles():
            tile, coor = res['tile'], res['coordinate']
            self.image_creator.add_tile(tile, coor)

        print('DONE')


    def create_colortiled_slide(self, color_coor_mapping, k):
        '''
        Given a mapping between color and coordinates of the image, creates bordered boxes all throughout the image.

        :param color_coor_mapping:
        :param k: just for naming purposes
        :return:
        '''

        sp = os.path.join(self.out_dir, '{}_unsupervised_results'.format(self.slide.name))
        pathlib.Path(sp).mkdir(parents=True, exist_ok=True)

        # now add grouped color borders
        for c, coors in color_coor_mapping.items():
            curr_color = re.search('color=\w+', c).group(0)
            curr_color = curr_color.split('=')[1]

            print('Outlining color {} on {} tiles'.format(curr_color, len(coors)))

            self.image_creator.add_borders(coors, color=unique_colors.BGR_COLORS[curr_color])

        cv2.imwrite(os.path.join(sp, '{:d}_colortiled_{}.jpg'.format(k, self.slide.name)), self.image_creator.image)


def get_dlfvs_of_slide(slide, model_path, tile_size=1024, min_non_blank_amt=0.0, blank_index=None):
    '''
    Get DLFVs of tiles present on a slide. If you want to use a blank filter (only choose tiles that have < x% blank),
    need to pass in index of the blank class in the confidence score output of the model.
    '''

    te = TileExtractor(slide, tile_size)

    datas = []
    coordinates = []

    model = load_model(model_path)

    for res in te.iterate_tiles():
        tile, coor = res['tile'], res['coordinate']

        if min_non_blank_amt == 0 or ModelUtils.get_conf_score(model, tile)[blank_index] <= (1 - min_non_blank_amt):
            data = ModelUtils.get_layer_data(model, tile, layer_name='global_average_pooling2d_1')
            datas.append(data)
            coordinates.append(coor)

    return datas, coordinates


def perform_colortiling(slide, out_dir, tile_size, datas, coordinates, k_values, linkage_method='complete'):
    scaled_data = MinMaxScaler(copy=False).fit_transform(datas)

    sct = SlideColortiler(slide, out_dir, tile_size=tile_size)

    # cycle through different k values
    for k in k_values:

        cluster_labels = AgglomerativeClustering(n_clusters=k, linkage=linkage_method).fit_predict(scaled_data)

        temp_df = pd.DataFrame({'Cluster': cluster_labels, 'Coor': coordinates})
        # dict mapping cluster to list of coordinates
        cluster_to_coordinates = temp_df.groupby('Cluster')['Coor'].apply(list).to_dict()

        # update dictionary key above specify color to use for each cluster
        color_gen = unique_colors.next_color_generator()
        for group in sorted(cluster_to_coordinates):
            color = next(color_gen)
            cluster_to_coordinates['{} (color={})'.format(group, color)] = cluster_to_coordinates.pop(group)

        sct.create_colortiled_slide(cluster_to_coordinates, k)


if __name__ == '__main__':
    model_path = '../models/brain_74_classes_VGG19.h5'
    slide_path = '../slide.svs'
    save_dir = '../out'
    k_values = [2, 3, 4, 5, 6, 7, 8, 9]

    ### DEFAULTS
    tile_size = 1024
    min_non_blank_amt = 0.4
    blank_index = 10  # required if min_non_blank_amt > 0
    linkage_method = 'complete'
    ###

    slide = Slide(slide_path, img_requirements={'compression': [70], 'mpp': None})

    dlfvs, coordinates = get_dlfvs_of_slide(
        slide, model_path, tile_size=tile_size, min_non_blank_amt=min_non_blank_amt, blank_index=blank_index)

    perform_colortiling(slide, save_dir, tile_size, dlfvs, coordinates, k_values, linkage_method=linkage_method)
